<?php
/**
 * @file
 * Defines a shooting score field type consisting of points and bullseyes.
 */

/**
 * Implements hook_field_info().
 */
function shooting_score_field_info() {
  return array(
    'shooting_score' => array(
      'label' => t('Shooting score'),
      'description' => t('This field stores shooting scores in the database as points and bullseyes.'),
      'instance_settings' => array(
        'min_points' => '0',
        'max_points' => '',
        'min_bullseyes' => '0',
        'max_bullseyes' => '',
      ),
      'default_widget' => 'shooting_score_textfields',
      'default_formatter' => 'shooting_score_default',
    ),
  );
}

/**
 * Implements hook_field_instance_settings_form().
 */
function shooting_score_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $values = array('points', 'bullseyes');

  $form['min_max'] = array(
    '#type' => 'fieldset',
    '#title' => t('Minimum & maximum values'),
    '#parents' => array('instance', 'settings'),
  );
  foreach ($values as $value) {
    $form['min_max']['min_' . $value] = array(
      '#type' => 'textfield',
      '#title' => t('Min @value', array('@value' => $value)),
      '#description' => t('The minimum value that should be allowed in the %value field. Must be a non-negative integer.', array('%value' => $value)),
      '#default_value' => $settings['min_' . $value],
      '#size' => 5,
      '#required' => TRUE,
      '#element_validate' => array('shooting_score_element_validate_non_negative_integer'),
    );
    $form['min_max']['max_' . $value] = array(
      '#type' => 'textfield',
      '#title' => t('Max @value', array('@value' => $value)),
      '#description' => t('The maximum value that should be allowed in the %value field. Must be a non-negative integer. Leave blank for no maximum.', array('%value' => $value)),
      '#default_value' => $settings['max_' . $value],
      '#size' => 5,
      '#element_validate' => array('shooting_score_element_validate_non_negative_integer', 'shooting_score_element_validate_max_min'),
    );
  }

  return $form;
}

/**
 * Custom form element validation handler for non-negative integers.
 *
 * @see element_validate_integer_positive().
 */
function shooting_score_element_validate_non_negative_integer($element, &$form_state) {
  $value = $element['#value'];

  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a non-negative integer.', array('%name' => $element['#title'])));
  }
}

/**
 * Custom form element validation handler for max values that must be >= min
 * values.
 */
function shooting_score_element_validate_max_min($element, &$form_state) {
  $value = $element['#value'];

  if ($value !== '') {
    // Get this element's type.
    $reverse_parents = array_reverse($element['#parents']);
    $element_name = explode('_', $reverse_parents[0]);
    $type = $element_name[1];

    // Get matching minimum value.
    $minimum = $form_state['values']['instance']['settings']['min_' . $type];

    if ($value < $minimum) {
      form_error($element, t('%name must be greater than or equal to the minimum value of @minimum.', array('%name' => $element['#title'], '@minimum' => $minimum)));
    }
  }
}

/**
 * Implements hook_field_validate().
 */
function shooting_score_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $settings = $instance['settings'];
  $label = $instance['label'];
  $values = array('points', 'bullseyes');

  // Ensure value is not outside of specified minimum and maximum values.
  foreach ($items as $delta => $item) {
    foreach ($values as $value) {
      if ($item[$value] != '') {
        if ($item[$value] < $settings['min_' . $value]) {
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => 'shooting_score_min_' . $value,
            'message' => t('%name: @value cannot be less than @min.', array('%name' => $label, '@value' => $value, '@min' => $settings['min_' . $value])),
          );
        }
        if (($settings['max_' . $value] != '') && ($item[$value] > $settings['max_' . $value])) {
          $errors[$field['field_name']][$langcode][$delta][] = array(
            'error' => 'shooting_score_max_' . $value,
            'message' => t('%name: @value cannot be greater than @max.', array('%name' => $label, '@value' => $value, '@max' => $settings['max_' . $value])),
          );
        }
      }
    }
  }
}

/**
 * Implements hook_field_widget_error().
 */
function shooting_score_field_widget_error($element, $error, $form, &$form_state) {
  // Get element value.
  $error_code = array_reverse(explode('_', $error['error']));
  $value = $error_code[0];

  form_error($element[$value], $error['message']);
}

/**
 * Implements hook_field_presave().
 */
function shooting_score_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  $settings = $instance['settings'];
  $values = array('points', 'bullseyes');

  // Convert empty values to minimum value.
  foreach ($items as $delta => $item) {
    foreach ($values as $value) {
      if ($item[$value] == '') {
        $items[$delta][$value] = $settings['min_' . $value];
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function shooting_score_field_is_empty($item, $field) {
  // No points means field is empty (regardless of bullseyes).
  if (!isset($item['points']) || ($item['points'] == '')) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_field_widget_info().
 */
function shooting_score_field_widget_info() {
  return array(
    'shooting_score_textfields' => array(
      'label' => t('Text fields'),
      'field types' => array('shooting_score'),
      'settings' => array(
        'points_size' => 4,
        'bullseyes_size' => 2,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function shooting_score_field_widget_settings_form($field, $instance) {
  $settings = $instance['widget']['settings'];
  $values = array('points', 'bullseyes');

  $form['size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Texfield sizes'),
    '#parents' => array('instance', 'widget', 'settings'),
  );
  foreach ($values as $value) {
    $form['size'][$value . '_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Size of @value textfield', array('@value' => $value)),
      '#default_value' => $settings[$value . '_size'],
      '#size' => 5,
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function shooting_score_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $settings = $instance['widget']['settings'];
  $values = array('points', 'bullseyes');

  foreach ($values as $value) {
    $element[$value] = array(
      '#type' => 'textfield',
      '#description' => t('@value', array('@value' => ucfirst($value))),
      '#default_value' => isset($items[$delta][$value]) ? $items[$delta][$value] : '',
      '#size' => $settings[$value . '_size'],
    );
  }

  // Add custom styling.
  $element += array(
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'shooting_score') . '/shooting_score.css',
      ),
    ),
  );

  return $element;
}

/**
 * Implements hook_field_formatter_info().
 */
function shooting_score_field_formatter_info() {
  return array(
    'shooting_score_default' => array(
      'label' => t('Default'),
      'field types' => array('shooting_score'),
      'settings' => array(
        'separator' => '.',
      ),
    ),
    'shooting_score_points' => array(
      'label' => t('Points'),
      'field types' => array('shooting_score'),
    ),
    'shooting_score_bullseyes' => array(
      'label' => t('Bullseyes'),
      'field types' => array('shooting_score'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function shooting_score_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  if ($display['type'] == 'shooting_score_default') {
    $element['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#description' => t('The character used to separate points and bullseyes.'),
      '#default_value' => $settings['separator'],
      '#size' => 2,
      '#maxlength' => 1,
      '#required' => TRUE,
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function shooting_score_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = '';

  if ($display['type'] == 'shooting_score_default') {
    $summary = t('Separator: @separator', array('@separator' => $settings['separator']));
  }

  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function shooting_score_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];
  $element = array();

  switch ($display['type']) {
    case 'shooting_score_default':
      // Display the points and bullseyes together.
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['points'] . $settings['separator'] . $item['bullseyes']);
      }
      break;
    case 'shooting_score_points':
      // Display just the points.
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['points']);
      }
      break;
    case 'shooting_score_bullseyes':
      // Display just the bullseyes.
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item['bullseyes']);
      }
      break;
  }

  return $element;
}

